from robot_pkg.Space import Grid
import unittest
import logging
class TestGrid(unittest.TestCase):
    def setUp(self):
        """
        The function to setup the class.
        """
        self.map = Grid()
        self.map.add_Position(1, 3, 4, 5)
        self.map.add_Position(2, 3, 4, 5)
        self.map.add_Position(3, 1000, 1000, 1)

    def test_Collision(self):
        """
        The function to test if the result of collision method
        is the expected one
        """
        self.assertEqual(self.map.Collision(1,3), False)
        self.assertEqual(self.map.Collision(1,2), True)
    

if __name__ == "__main__":
    unittest.main()
