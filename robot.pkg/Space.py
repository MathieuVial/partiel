"""Class qui modelise un environnement """
from math import sqrt


class Grid:
    def __init__(self):
        self.grid = {}

    def add_Position(self, id, position_x, position_y, position_z):
        """fonction qui ajoute une position"""
        try:
            self.grid[id] = (position_x, position_y, position_z)
            return True
        except:
            return "erreur"

    def remove_Position(self, id):
        """fonction qui retire une position"""
        try:
            del self.grid[id]
            return True
        except:
            return "erreur"

    def List_Position(self):
        """fonction qui retourne la liste des position"""
        try:
            List_values = []
            for valeur in self.grid.values():
                List_values = List_values + [valeur]
            return List_values
        except:
            return "erreur"

    def Position(self, id):
        """fonction de gestion de la position"""
        try:
            positon = self.grid.get(id)
            return positon
        except:
            return "erreur"

    def Distance(self, id_1, id_2):
        """fonction de gestion de la distance"""
        try:
            positon_1 = self.grid.get(id_1)
            positon_2 = self.grid.get(id_2)
            distance = sqrt(
                ((positon_1[0] - positon_2[0]) ** 2)
                + ((positon_1[1] - positon_2[1]) ** 2)
            )
            return distance
        except:
            return "erreur"

    def Collision(self, id1, id2):
        """fonction de gestion de la collision"""
        try:
            rayon1 = self.grid.get(id1)[2]
            rayon2 = self.grid.get(id2)[2]
            collision = False
            if self.Distance(id1, id2) < rayon1 + rayon2:
                collision = True
            return collision
        except:
            return "erreur"
