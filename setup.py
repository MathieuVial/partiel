""" This file is the setup file
    Use to build PyPi wheel pkg
"""

from setuptools import setup

setup(
    name="Partiel_Mathieu",
    version="0.0.1",
    author="Mathieu VIAL",
    packages=["robot_pkg"],
    description="ce programme est la pour tester mes connaissances en admin sys",
    license="GNU GPLv3",
    python_requires=">=2.4",
)
